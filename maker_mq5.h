#ifndef _MAKER_MQ5_
#define _MAKER_MQ5_

extern double R0;

/**
    Note: Reads the value of the sensor at the given analog pin.

    @param
      analogPin is the pin from which to read the value.

    @return
      the value read from the analogPin. (Voltage read from the pin)
*/
static uint8_t mq5GetSensorValue(uint8_t analogPin);

/**
    Note: Calculates the sensor volt value based on the value read from the analog pin.

    @param
      analogPin is the pin from which to read the sensor value.

    @return
      the sensor volt value.
*/
double maker_mq5GetSensorVolt(uint8_t analogPin);

/**
    Note: Calculates the RS value in the air based on the sensor volt value.

    @param
      analogPin is the pin from which to read the sensor value.

    @return
      the sensor resistance in air.
*/
double maker_mq5GetRSAir(uint8_t analogPin);

/**
    Note: Gets the average data by testing 100 times in a row and then calculates R0 by using that data.

    @param
      analogPin is the pin from which to read the sensor value.

    @return
      nothing. It just updates the R0 global variable each time the function gets called.
*/
void maker_mq5GetEnvironmentR0(uint8_t analogPin);

/**
    Note: Calcualtes the ratio of RS/R0.

    @param
      analogPin is the pin form which to read the sensor value.

    @return
      the ratio of RS/R0.
*/
double maker_mq5GetRatio(uint8_t analogPin);

#endif
