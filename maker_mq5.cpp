/*
 * Library: maker_mq5.h
 *
 * Organization: MakerRobotics
 * Autors: Maksim Jovovic
 *
 * Date: 24.11.2017.
 * Test: Arduino UNO
 *
 * Note:
 * 
 */

#include "Arduino.h"
#include "maker_mq5.h"
#include "stdint.h"

double R0;

/*Reads the value of the sensor at the given analog pin.*/
static uint8_t _mq5GetSensorValue(uint8_t analogPin)
{
    return analogRead(analogPin);
}

/*Calculates the sensor volt value based on the value read from the analog pin.*/
double maker_mq5GetSensorVolt(uint8_t analogPin){
  return (double)_mq5GetSensorValue(analogPin) / 1024 * 5.0;
}

/*Calculates the RS value in the air based on the sensor volt value.*/
double maker_mq5GetRSAir(uint8_t analogPin)
{
   double sensorVolt = maker_mq5GetSensorVolt(analogPin);
   return (5.0 - sensorVolt) / sensorVolt;
} 

/*Gets the average data by testing 100 times in a row and then calculates R0 by using that data.*/
void maker_mq5GetEnvironmentR0(uint8_t analogPin)
{
   double sensorValue = 0;
   for(int i = 0; i < 100; i++)
   {
       sensorValue += analogRead(analogPin);
   }
   sensorValue /= 100.0;

   double sensorVolt = sensorValue / 1024 * 5.0;
   double RSAir = (5.0 - sensorVolt) / sensorVolt;
   R0 = RSAir / 6.5;
}

/*Calcualtes the ratio of RS/R0.*/
double maker_mq5GetRatio(uint8_t analogPin)
{
    return maker_mq5GetRSAir(analogPin) / R0;
}
